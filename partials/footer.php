<footer class="[ l-footer ][ u-background--dark ]">
    <div class="container">
        <div class="row columns">

            <div class="l-footer__social">
                <h3 class="[ text--bold text--16 ]">Follow Us</h3>
                <ul class="list--bare site--clearfix">
                    <li>
                        <a href="#"><?php echo EP\Images::get_image('/youtube.png', 39, 39); ?></a>
                    </li>
                    <li>
                        <a href="#"><?php echo EP\Images::get_image('/twitter.png', 39, 39); ?></a>
                    </li>
                    <li>
                        <a href="#"><?php echo EP\Images::get_image('/facebook.png', 39, 39); ?></a>
                    </li>
                    <li>
                        <a href="#"><?php echo EP\Images::get_image('/dribble.png', 39, 39); ?></a>
                    </li>
                    <li>
                        <a href="#"><?php echo EP\Images::get_image('/rrs.png', 39, 39); ?></a>
                    </li>
                </ul>
            </div>

            <nav class="l-footer__nav" aria-label="Footer Menu">
                <ul class="[ list--bare site--clearfix ][ text--16 ]">
                    <li><a href="#">Terms and disclaimer</a></li>
                    <li><a href="#">Site Map</a></li>
                    <li><a href="#">Accessibility</a></li>
                    <li><a href="#">Privacy</a></li>
                </ul>
            </nav>

            <div class="l-footer__copy">
                <p class="text--16">&copy;2015 Perfect Council - Customer Services, Council House, 23 Council Lane, Town Name, PC1 4TN<br>
                Opening hours: Mon-Fri, 9am-5pm, Telephone: (012) 3456 7890 or 098 7654 3210</p>
                <p class="text--16 text--grey">Handcrafted by <a href="#">Spacecraft</a>, Powered by <a href="#">Jadu</a>.</p>
            </div>

        </div>
    </div>
</footer>