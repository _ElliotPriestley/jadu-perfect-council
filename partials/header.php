<header class="[ l-header ][ text--demi ]">
    <div class="container">
        <div class="row columns">
            <h1 class="l-header__logo">
                <a href="#">Perfect Council</a>
            </h1>

            <div href="#" class="l-header__burger">
                <span></span>
                <span></span>
                <span></span>
            </div>

            <nav class="l-header__nav" aria-label="Menu">
                <ul class="[ list--bare ][ site--clearfix ]">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">A-Z Council services</a></li>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Contact</a></li>
                    <li class="text--bold"><a href="#">My Account/Register</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
