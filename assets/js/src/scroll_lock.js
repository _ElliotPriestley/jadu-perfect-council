export { scroll_lock };

// Slideshow Functions

let $ = jQuery.noConflict();

let scroll_lock = {

    init () {

        this.scroll_lock.init();

    },

    scroll_lock : {

        init () {

        },

        lock ( callback = false ) {

            $( '.site' ).css( 'top', -$( window ).scrollTop());
            $( 'html' ).addClass( 'scroll-lock' );

            if( !callback )
                return;

            callback();

        },

        unlock () {

            if( $( 'html' ).hasClass( 'scroll-lock' ) ) {

                var reset_scroll = parseInt( $( '.site' ).css( 'top' ) ) * -1;

                $( 'html' ).removeClass('scroll-lock');
                $( '.site' ).css( 'top', 'auto' );
                $( 'html, body' ).animate({
                    scrollTop: reset_scroll
                }, 0);

            }

        },
    },
}