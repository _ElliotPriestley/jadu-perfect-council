// Class import
// import Example from './examples';
// Var import
import { wait_for_final_event } from './helper';
import { plugins } from './plugins';
import { slideshow } from './slideshow';
import { scroll_lock } from './scroll_lock';

(function( $, document ) {

    $( document ).ready( function() {

        app_init.ready();

    });

    $( window ).load( function() {
    });

    $( window ).scroll( function() {
    });

    $( window ).resize( function() {

        wait_for_final_event( function() {

        }, 300, 'init');

    });

    const app_init = {

        ready : function() {

            // Scroll Lock
            scroll_lock.init();

            // Plugins
            plugins.init();

            // Slideshow
            slideshow.init();

            // App
            app.init();

        }
    }

    // App Functions

    let app = {

        init : function() {

            this.mobile_menu.init();
            this.scroll_to.init();
            this.tabs.init();

        },

        mobile_menu : {

            init () {

                $('.l-header__burger').on('click', function() {

                    if( $('body').hasClass('menu-active') ) {

                        $('body').removeClass('menu-active');

                    } else {

                        $('body').addClass('menu-active');

                    }

                    return false;
                });

            }

        },

        scroll_to : {

            init () {

                $('.hero__scroll-down').on('click', function() {

                    $('html, body').animate({
                        scrollTop: $('.js-scroll-to').offset().top - $('.l-header').outerHeight()
                    }, 500);

                    return false;
                });

            }

        },

        tabs : {

            init () {

                $('.tabs__tab').on('click', function() {

                    if( $(this).hasClass('is-active') ) {

                        $('.tabs__tab').removeClass('is-active');
                        $('.tab__hidden').removeClass('is-visible');

                    } else {

                        var this_data = $(this).attr('data-tab');

                        $('.tabs__tab').removeClass('is-active');
                        $(this).addClass('is-active');

                        $('.tab__hidden').removeClass('is-visible');
                        $('.tab__hidden[data-tab="' + this_data + '"]').addClass('is-visible');

                    }

                    return false;

                });

            }

        }
    }

}(jQuery, document));