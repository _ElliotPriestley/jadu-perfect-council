export { plugins };

// Plugin Functions

let $ = jQuery.noConflict();

let plugins = {

    init () {

        // Foundation must go first as it is a dependancy
        this.foundation.init();

        this.lazy_sizes.init();
        this.match_height.init();
        this.perfect_scrollbar.init();
        this.inview.init();

    },

    foundation : {

        init () {

            $( document ).foundation();

        }

    },

    lazy_sizes : {

        init () {

            window.lazySizesConfig = window.lazySizesConfig || {};
            lazySizesConfig.expand = 1500;
            lazySizesConfig.loadHidden = false;

        },

        load () {
        }

    },

    match_height : {

        init : function() {

            $( '.js-match-height' ).matchHeight();

        }

    },

    perfect_scrollbar : {

        init () {

            if( $( '.js-scrollbar' ).length > 0 ) {

                $( '.js-scrollbar' ).perfectScrollbar({
                    wheelPropagation: true
                });
            }

        }

    },

    inview : {

        init : function() {

            if( $( '.js-animate' ).length > 0 ) {

                $( '.js-animate' ).on( 'inview', function( event, isInView ) {

                    if ( isInView ) {

                        if( !$( this ).hasClass( 'is-inview' ) ) {
                            $( this ).addClass( 'is-inview' );
                        }

                    }
                });
            }

        }
    }
}