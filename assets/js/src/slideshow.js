export { slideshow };

// Plugin Functions

let $ = jQuery.noConflict();

let slideshow = {

    init () {

        this.slideshow.init();

    },

    slideshow : {

        init () {

            $('.slideshow--top-tasks').flickity({
                // options
                cellAlign: 'left',
                contain: true,
                pageDots: false,
                wrapAround: true
            });

            $('.slideshow--latest-news').flickity({
                // options
                cellAlign: 'left',
                contain: true,
                pageDots: false,
                wrapAround: true
            });

            $('.slideshow--testimonials').flickity({
                // options
                cellAlign: 'left',
                contain: true,
                pageDots: true,
                prevNextButtons: false,
                wrapAround: true
            });

        }

    }
}