<?php

    namespace EP;

    /**
     * Demo Class.
     *
     * @class       EP\Demo
     * @version     1.0.0
     */

    class Slideshows {

        public function __construct() {

        }

        public static function get_top_tasks_slider() {

        ?>
        <div class="slideshow slideshow--top-tasks">
            <?php for ($x = 0; $x <= 1; $x++) { ?>
            <div class="[ slideshow__slide ][ medium-6 large-4 xlarge-3 ]">
                <div class="[ slideshow__visible ][ js-match-height ]">
                    <figure>
                        <?php echo Images::get_image('/rubbish_and_recycling.png', 306, 196); ?>
                    </figure>
                    <div class="[ align--center ][ text--34 text--demi ][ js-animate ]">Rubbish and recycling</div>
                </div>
                <div class="slideshow__hidden">
                    <figure>
                        <?php echo Images::get_image('/24_7.png', 78, 65); ?>
                    </figure>
                    <nav aria-label="Rubbish and recycling menu <?php echo $x; ?>">
                        <ul class="list--bare">
                            <li><a href="#">Pay for council tax</a></li>
                            <li><a href="#">Check your account</a></li>
                            <li><a href="#">Change of address</a></li>
                            <li><a href="#">Council tax discounts</a></li>
                            <li><a href="#">Show more tasks</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="[ slideshow__slide ][ medium-6 large-4 xlarge-3 ]">
                <div class="[ slideshow__visible ][ js-match-height ]">
                    <figure>
                        <?php echo Images::get_image('/tax.png', 306, 196); ?>
                    </figure>
                    <div class="[ align--center ][ text--34 text--demi ][ js-animate ]">Council tax</div>
                </div>
                <div class="slideshow__hidden">
                    <figure>
                        <?php echo Images::get_image('/24_7.png', 78, 65); ?>
                    </figure>
                    <nav aria-label="Tax menu <?php echo $x; ?>">
                        <ul class="list--bare">
                            <li><a href="#">Pay for council tax</a></li>
                            <li><a href="#">Check your account</a></li>
                            <li><a href="#">Change of address</a></li>
                            <li><a href="#">Council tax discounts</a></li>
                            <li><a href="#">Show more tasks</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="[ slideshow__slide ][ medium-6 large-4 xlarge-3 ]">
                <div class="[ slideshow__visible ][ js-match-height ]">
                    <figure>
                        <?php echo Images::get_image('/parking.png', 306, 196); ?>
                    </figure>
                    <div class="[ align--center ][ text--34 text--demi ][ js-animate ]">Parking</div>
                </div>
                <div class="slideshow__hidden">
                    <figure>
                        <?php echo Images::get_image('/24_7.png', 78, 65); ?>
                    </figure>
                    <nav aria-label="Parking menu <?php echo $x; ?>">
                        <ul class="list--bare">
                            <li><a href="#">Pay for council tax</a></li>
                            <li><a href="#">Check your account</a></li>
                            <li><a href="#">Change of address</a></li>
                            <li><a href="#">Council tax discounts</a></li>
                            <li><a href="#">Show more tasks</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="[ slideshow__slide ][ medium-6 large-4 xlarge-3 ]">
                <div class="[ slideshow__visible ][ js-match-height ]">
                    <figure>
                        <?php echo Images::get_image('/report.png', 306, 196); ?>
                    </figure>
                    <div class="[ align--center ][ text--34 text--demi ][ js-animate ]">Report<br> a problem</div>
                </div>
                <div class="slideshow__hidden">
                    <figure>
                        <?php echo Images::get_image('/24_7.png', 78, 65); ?>
                    </figure>
                    <nav aria-label="Report a problem menu <?php echo $x; ?>">
                        <ul class="list--bare">
                            <li><a href="#">Pay for council tax</a></li>
                            <li><a href="#">Check your account</a></li>
                            <li><a href="#">Change of address</a></li>
                            <li><a href="#">Council tax discounts</a></li>
                            <li><a href="#">Show more tasks</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php

        }

        public static function get_latest_news_slider() {

        ?>
        <div class="[ slideshow slideshow--latest-news ][ mb40 ]">
            <?php for ($x = 0; $x <= 1; $x++) { ?>
            <div class="[ slideshow__slide ][ medium-6 xlarge-4 ]">
                <figure class="image--size-cover">
                    <?php echo Images::get_image('/tab1.jpg', 531, 340); ?>
                </figure>
                <h4 class="[ align--center ][ text--demi ][ js-match-height ][ js-animate ]">Council gets ready for cold weather </h4>
                <a href="#">Read more</a>
            </div>
            <div class="[ slideshow__slide ][ medium-6 xlarge-4 ]">
                <figure class="image--size-cover">
                    <?php echo Images::get_image('/tab2.jpg', 532, 340); ?>
                </figure>
                <h4 class="[ align--center ][ text--demi ][ js-match-height ][ js-animate ]">Council tax to be frozen despite government budget cuts this year</h4>
                <a href="#">Read more</a>
            </div>
            <div class="[ slideshow__slide ][ medium-6 xlarge-4 ]">
                <figure class="image--size-cover">
                    <?php echo Images::get_image('/tab3.jpg', 533, 340); ?>
                </figure>
                <h4 class="[ align--center ][ text--demi ][ js-match-height ][ js-animate ]">How to keep warm when it’s cold</h4>
                <a href="#">Read more</a>
            </div>
            <?php } ?>
        </div>
        <?php

        }

        public static function get_testimonials_slider() {

        ?>
        <div class="[ slideshow slideshow--testimonials ][ mb30 ]">
            <?php for ($x = 0; $x <= 2; $x++) { ?>
            <div class="[ slideshow__slide ]">
                <h4 class="[ text--demi text--40 ][ mb30 ]">Corporate message comes here</h4>
                <div class="text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.In quis augue et elit dignissim vulputate nec ut tortor. Sed pulvinar turpis eu pulvinar fringilla. Quisque faucibus dui purus, id aliquam ipsum laoreet non.</p>
                    <p>Maecenas pharetra sit amet <a href="#">link style</a> ac bibendum.</p>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php

        }

    }

    // Call
    // EP\Modules::Modules();