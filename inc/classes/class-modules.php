<?php

    namespace EP;

    /**
     * Demo Class.
     *
     * @class       EP\Demo
     * @version     1.0.0
     */

    class Modules {

        public function __construct() {

        }

        public static function get_search() {

        ?>
        <div class="search">
            <form class="[ search__form ][ site--clearfix ]" aria-label="Search">
                <input type="text" name="search" placeholder="Search website eg. council tax" aria-label="Search input">
                <input type="submit" value="Search">
            </form>
        </div>
        <?php

        }

        public static function get_twitter_feed() {

        ?>
        <div class="[ twitter-feed ][ js-scrollbar ]">

            <h3 class="[ twitter-feed__title ][ text--demi text--26 ]">Latest tweets</h3>

            <?php for ($x = 0; $x <= 2; $x++) { ?>

            <div class="[ twitter-feed__tweet ][ text--18 ]">
                <p>A really interesting tweet about something, read more: <a href="#">interestinglink.com/?a-link-about-som…</a>
                <strong>About 2 hours ago</strong></p>
            </div>

            <div class="[ twitter-feed__tweet ][ text--18 ]">
                <p>RT <a href="#">@SuperCoolTweeter</a>: Did you know you could do this? #innovation <a href="#">@Twitter</a> <a href="#">#LearnNewStuff</a> <a href="#">learnthings.co.uk/ news/super-interesting…</a><br>
                <strong>About 1 hour ago</strong></p>
            </div>

            <?php } ?>

        </div>
        <?php

        }

    }

    // Call
    // EP\Modules::Modules();