<?php

    namespace EP;

    if ( ! defined( 'ABSPATH' ) ) exit;

    /**
     * Demo Class.
     *
     * @class       EP\Demo
     * @version     1.0.0
     */

    class Demo {

        public function __construct() {

        }

        public static function function_name() {

        }

    }

    // Call
    // EP\Demo::function_name();