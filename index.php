<?php include( 'header.php' ); ?>

<div class="hero">

    <div class="[ hero__image ][ image--size-cover ]">
        <?php echo EP\Images::get_image('/hero.jpg', 1600, 717); ?>
    </div>

    <div class="container">
        <div class="row">

            <?php echo EP\Modules::get_search(); ?>

            <?php echo EP\Slideshows::get_top_tasks_slider(); ?>

        </div>
    </div>

    <a href="#" class="hero__scroll-down">
        <?php echo EP\Images::get_image('/scroll-down.jpg', 98, 98); ?>
    </a>

</div>

<div class="[ js-scroll-to ][ u-background--green ][ padding-top--80 padding-bottom--100 ]">
    <div class="container">
        <div class="row">
            <div class="column">
                <div class="[ align--center ][ text--22 text--demi ][ mb10 ][ js-animate ]">Most popular</div>
                <div class="[ align--center ][ text--45 text--demi ][ margin-bottom--60 ][ js-animate ]">Council services</div>
            </div>
        </div>
        <div class="[ row ][ tabs ][ margin-bottom--40 ]">
            <div class="[ columns medium-6 xlarge-3 ][ tabs__tab ][ js-animate ]" data-tab="1">
                <div class="[ tabs__visible ][ align--center ][ text--32 text--demi ][ js-match-height ]">Rubbish and recycling</div>
            </div>
            <div class="[ columns medium-6 xlarge-3 ][ tabs__tab ][ js-animate ]" data-tab="2">
                <div class="[ tabs__visible ][ align--center ][ text--32 text--demi ][ js-match-height ]">Parking and travel</div>
            </div>
            <div class="[ columns medium-6 xlarge-3 ][ tabs__tab ][ js-animate ]" data-tab="3">
                <div class="[ tabs__visible ][ align--center ][ text--32 text--demi ][ js-match-height ]">Streets and neighbourhood</div>
            </div>
            <div class="[ columns medium-6 xlarge-3 ][ tabs__tab ][ js-animate ]" data-tab="4">
                <div class="[ tabs__visible ][ align--center ][ text--32 text--demi ][ js-match-height ]">Housing and council tax</div>
            </div>

            <div class="site--clearfix"></div>

            <div class="[ columns ][ tab__hidden ][ text--demi ]" data-tab="1">
                <nav aria-label="Rubbish Tab Menu">
                    <ul class="[ list--bare ][ site--clearfix ]">
                        <li><a href="#">Council housing</a></li>
                        <li><a href="#">Council tax</a></li>
                        <li><a href="#">Housing benefit and council tax support</a></li>
                        <li><a href="#">Finding a home</a></li>
                    </ul>
                </nav>
            </div>
            <div class="[ columns ][ tab__hidden ][ text--demi ]" data-tab="2">
                <nav aria-label="Parking Tab Menu">
                    <ul class="[ list--bare ][ site--clearfix ]">
                        <li><a href="#">Council tax</a></li>
                        <li><a href="#">Council housing</a></li>
                        <li><a href="#">Housing benefit and council tax support</a></li>
                        <li><a href="#">Finding a home</a></li>
                    </ul>
                </nav>
            </div>
            <div class="[ columns ][ tab__hidden ][ text--demi ]" data-tab="3">
                <nav aria-label="Streets and neighbourhood Tab Menu">
                    <ul class="[ list--bare ][ site--clearfix ]">
                        <li><a href="#">Housing benefit and council tax support</a></li>
                        <li><a href="#">Council housing</a></li>
                        <li><a href="#">Council tax</a></li>
                        <li><a href="#">Finding a home</a></li>
                    </ul>
                </nav>
            </div>
            <div class="[ columns ][ tab__hidden ][ text--demi ]" data-tab="4">
                <nav aria-label="Housing and council tax Tab Menu">
                    <ul class="[ list--bare ][ site--clearfix ]">
                        <li><a href="#">Finding a home</a></li>
                        <li><a href="#">Council housing</a></li>
                        <li><a href="#">Council tax</a></li>
                        <li><a href="#">Housing benefit and council tax support</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="columns">
                <p class="[ align--center ][ arrow--down ][ text--22 text--demi ][ js-animate ]">Can’t find it?<br>
                See other council services</p>
            </div>
        </div>
    </div>
</div>

<div class="[ u-background--dark ][ padding-top--100 padding-bottom--100 ]">
    <div class="container">
        <div class="row">
            <div class="[ columns large-4 ][ seperator seperator--first ]">
                <h3 class="[ text--26 text--demi ][ mb10 ][ js-animate ]">Community</h3>
                <p class="[ text--22 text--medium ][ js-animate ]">Community projects, Volunteering, Ward forums</p>
            </div>
            <div class="[ columns large-4 ][ seperator ]">
                <h3 class="[ text--26 text--demi ][ mb10 ][ js-animate ]">Business</h3>
                <p class="[ text--22 text--medium ][ js-animate ]">Regeneration, Tourism, Business Partnership</p>
            </div>
            <div class="[ columns large-4 ][ seperator ]">
                <h3 class="[ text--26 text--demi ][ mb10 ][ js-animate ]">The Council</h3>
                <p class="[ text--22 text--medium ][ js-animate ]">Councillors, Committees, Priorities</p>
            </div>
        </div>
    </div>
</div>

<div class="[ padding-top--60 padding-bottom--60 ][ align--center ]">
    <div class="container container--full">
        <div class="row column">
            <h2 class="[ text--22 text--demi ][ mb30 ][ js-animate ]">News & Events</h2>

            <?php echo EP\Slideshows::get_latest_news_slider(); ?>

            <a href="#" class="[ display--inline-block ][ arrow--right-dark ][ text--22 text--demi ][ js-animate ]">Read more News & Events</a>
        </div>
    </div>
</div>

<div class="[ u-background--off-white ][ padding-top--90 padding-bottom--120 ]">
    <div class="container">
        <div class="row">
            <div class="columns xmedium-6">
                <?php echo EP\Slideshows::get_testimonials_slider(); ?>
            </div>

            <div class="columns xmedium-6">
                <?php echo EP\Modules::get_twitter_feed(); ?>
            </div>
        </div>
    </div>
</div>

<?php include( 'footer.php' ); ?>